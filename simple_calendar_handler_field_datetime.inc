<?php

/**
 * @file
 * Views handler for simple_calendar.
 */

/**
 * Extends the basic views date handler to cope with SQL dates
 */
Class simple_calendar_handler_field_datetime extends views_handler_field_date {

  /**
   * Changes the MySQL datetime field into a UNIX timestamp required by views
   * @param $values Array An array of objects containing parameters for this view
   */
  function pre_render($values) {
    foreach ($values as $value) {
      $value -> {$this -> field_alias} = strtotime($value -> {$this -> field_alias} . ' UTC');
    }
  }

}
