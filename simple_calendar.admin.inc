<?php

/**
 * @file
 * Calendar build functionality for simple_calendar.
 */

/**
 * Returns the HTML required to build a calendar for a given month
 *
 * @param $month Mixed Any value that is accepted by the DateTime constructor
 * @param $heading_style String The HTML heading level to assign to the month name
 * @param $table_class String The CSS class that will be added to the <table> element
 * @return String
 */
function simple_calendar_get_month_data($month = "this month") {
  global $base_path, $base_url;

  $date_time = new DateTime($month);
  $number_of_days_in_month = (int)$date_time -> format('t');
  $previous_month = clone $date_time;
  $previous_month -> sub(new DateInterval('P1M'));
  $number_of_days_in_previous_month = $previous_month -> format('t');
  $first_day_of_month = new DateTime("first day of $month");
  $last_day_of_month = new DateTime("last day of $month");

  // Query database just once for events that fall in the month requested
  $events = array();
  $rewritten_sql = db_rewrite_sql("SELECT n.nid,n.title,{simple_calendar}.event_date FROM {node} n INNER JOIN {simple_calendar} ON n.vid={simple_calendar}.vid WHERE {simple_calendar}.event_date BETWEEN '%s' AND '%s' ORDER BY {simple_calendar}.event_date", 'n', 'nid');
  $db_result = db_query($rewritten_sql, $first_day_of_month -> format('Y-m-d'), $last_day_of_month -> format('Y-m-d'));
  if (!$db_result) {
    drupal_set_message('Could not look up events in the database', 'error');
  }
  while ($data = db_fetch_array($db_result)) {
    $data['url'] = $base_url . $base_path . drupal_lookup_path('alias', 'node/' . $data['nid']);
    $date_key = strstr($data['event_date'], ' ', TRUE);
    $events[$date_key][] = $data;
  }

  // Create two utility arrays of dates
  $array_of_days_in_month = range(1, $number_of_days_in_month);
  $last_seven_days_of_previous_month = range($number_of_days_in_previous_month - 7, $number_of_days_in_previous_month);

  // This loop checks whether any events exist on each day in the month being processed, and if so injects them into the DateTime object
  foreach ($array_of_days_in_month as &$day_in_month) {
    $day_in_question = new DateTime($day_in_month . ' ' . $date_time -> format('F'));
    if (array_key_exists($day_in_question -> format('Y-m-d'), $events)) {
      $day_in_question -> events = $events[$day_in_question -> format('Y-m-d')];
    }
    $day_in_month = $day_in_question;
  }
  
  // This loop creates DateTime entries for the last few days of the previous month
  foreach ($last_seven_days_of_previous_month as &$day_in_previous_month) {
    $day_in_previous_month = new DateTime($day_in_previous_month . ' ' . $previous_month -> format('F'));
  }
  
  // The $calendar_build variable is an array with keys that corresponding to table cell positions and values that correspond to days of the month
  $calendar_build = array_combine(range((int)$first_day_of_month -> format('N'), (int)$number_of_days_in_month + (int)$first_day_of_month -> format('N') - 1), $array_of_days_in_month);

  // Add the days from the previous month into the first row
  for ($i = 1; $i < $first_day_of_month -> format('N'); $i++) {
    array_unshift($calendar_build, $last_seven_days_of_previous_month[count($last_seven_days_of_previous_month) - $i]);
  }

  // Chunk the calendarBuild array in a multidimensional array in 7 day pieces
  $calendar_build = array_chunk($calendar_build, 7);

  return $calendar_build;
}
