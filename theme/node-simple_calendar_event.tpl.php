<?php
$datetime = new DateTime($node->event_date['day'].'-'.$node->event_date['month'].'-'.$node->event_date['year']);
?>
<div class="node-content" itemscope itemtype="http://schema.org/Event">
<h1 itemprop="name"><?php print $node->title;?></h1>
<div class="date" itemprop="startDate" content="<?php print $datetime->format('Y-m-d\TH:i')?>"><label for="event-date">Date:</label><span id="event-date"><?php print $datetime->format('l, jS F Y');?></span></div>
<?php if(!empty($node->event_time)):?>
<div class="time"><label for="event-time">Time:</label><span id="event-time"><?php print $node->event_time;?></span></div>
<?php endif;?>
<?php if(!empty($node->location)):?>
<div class="location" itemprop="location"><label for="event-location">Location:</label><span id="event-location"><?php print $node->location;?></span></div>
<?php endif;?>
<?php if(!empty($node->body)):?>
<div class="event-description" itemprop="description"><?php print $node->body;?></div>
<?php endif;?>
<?php if(!empty($node->event_url)):?>
<div class="url"><label for="event-url">More information:</label><span id="event-url"><a itemprop="url" href="<?php print strpos($node->event_url,'http://') === FALSE ? 'http://'.$node->event_url : $node->event_url;?>"><?php print $node->event_url;?></a></span></div>
<?php endif;?>
</div>
