<?php

/**
 * @file
 * Views implementation for simple_calendar.
 */

function simple_calendar_views_handlers() {
  return array(
    'handlers' => array(
      'simple_calendar_handler_field_datetime' => array(
        'parent' => 'views_handler_field_date',
      )
    )
  );
}

function simple_calendar_views_data() {
    
  $data = array();
  $data['simple_calendar']['table']['group']  = t('Event');  
  
  // tables + fields that can be used for SQL Joins
  $data['simple_calendar']['table']['join'] = array(
    'node_revisions' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  // Location
  $data['simple_calendar']['event_location'] = array(
    'group' => t('Event'),
    'title' => t('Event location'),
    'help' => t('The location of an event.'),
    'field' => array(
      'handler' => 'views_handler_field'
     ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Event Date        
  $data['simple_calendar']['event_date'] = array(
    'group' => t('Event'),
    'title' => t('Event date'),
    'help' => t('Date of the event.'),

    'field' => array(
      'handler' => 'simple_calendar_handler_field_datetime',
     ),
     'filter' => array(
      'handler' => 'views_handler_filter',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_date',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort_date',
     ),
  );
  
  // URL        
  $data['simple_calendar']['event_url'] = array(
    'group' => t('Event'),
    'title' => t('Event URL'),
    'help' => t('A link to a location with more information about the event.'),

    'field' => array(
      'handler' => 'views_handler_field_url',
     ),
     'filter' => array(
      'handler' => 'views_handler_filter',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );
        
    $data['simple_calendar']['table']['base'] = array(
    'field' => 'vid',
    'title' => t('Event node'),
    'help' => t("Simple Calendar events with date, location and url information."),
    'weight' => -9,
  );

  // Relationship to the 'Node revision' table
  $data['simple_calendar']['vid'] = array(
    'title' => t('Node revision'),
    'help' => t('The particular node revision to which the event details are attached.'),
    'relationship' => array(
      'label' => t('Node revision'),
      'base' => 'node_revisions',
      'base field' => 'vid',
      'skip base' => array('node', 'node_revisions'),
    ),
  );

  // Relationship to the 'Node' table
  $data['simple_calendar']['nid'] = array(
    'title' => t('Node'),
    'help' => t('The particular node to which the event details are attached.'),
    'relationship' => array(
      'label' => t('Node'),
      'base' => 'node',
      'base field' => 'nid',
      'skip base' => array('node', 'node_revisions'),
    ),
  );
  return $data;
}
